# Perthensis design document

## Overall design

- Data types and functions similar to Jelly
    - Strings are character arrays?
    - Booleans are numbers
    - Bigint support?
    - Rational support or int/float?
- Syntax like Pyth
    - Functional/imperative paradigms
- Jelly codepage or custom?
- 1-indexed arrays like Jelly
- Pyth-like implicit printing for every non-assign statement
- Implicit vectorization like Jelly
- Implicit conversions of number to range

## Syntax elements

- If, foreach, while
    - By default single statement
    - Character for two-statement block
    - Character for n-statement block
- Line comment, block comment
- Define lambda
    - Immediately called for efficiency

## Literals

- Jelly-like numbers
    - Integers, decimals, exponents, complex
    - Defaults for omitted parts
    - Fraction literals?
- Unicode string
    - In UTF-8 mode, a UTF-8 string literal
    - In codepage mode, uses compression:
        - `0xxxxxxx` = ASCII character `xxxxxxx`
        - `10xxxxxx` + 1 byte = 14-bit Unicode character
        - `11xxxxxx` + 2 bytes = 21-bit Unicode character
- Dictionary string
    - Somewhat like Jelly, but with all words lowercased
    - More customization of words, such as capitalize/uppercase?
- Codepage index list
    - Like Jelly
- Large-base number?
- Custom-base list?
    - Like Pyth string compression, but as a number list
    - Lower bound or always 0/1?
- Zlib string?
    - Minimal headers, etc.

## Variables

- Preinitialized variables:
    - Alphabet/Digits/Base64/etc.
- Inputs
- Autoinitializing variables
    - Initialized on first call
        - Determined compile time?
    - Like a function `get_or_init_X(() => ...)`
    - Option to reinitialize every time or only on first run
        - Modifier or a special variable?
- Some variables might be assignments when in statement position

## Modifiers

- Like Jelly's quicks or Pyth's `DFILMRV#`
- Some placed immediately after a function
    - Vectorize
        - Simultaneously maps all arguments
    - Root-of-map
        - Allows for complex maps by marking the start of the lambda
- Some placed in place of an argument (e.g. map)
    - Invariant
        - Check if the function returns this argument
    - Multiple map modifiers
        - Allow for mapping complex functions
    - Deep map
        - Like many Jelly functions
        - Maybe needs to be default or a directive?
    - Filter
        - Map over this argument, return for truthy
        - Negative filter modifier?
    - Find-first
        - Map/iterate over this argument, return first truthy
    - Find-n
        - Map/iterate over this argument, return first n truthy
    - Sort
        - Map over this argument, sort by return value
- "Special" modifiers that act differently depending on function

## Directives

- Change parse modes
- Digit mode
    - Parses consecutive digits as distinct numbers

## Builtins

- Array manipulation
    - wrap, pair, list-of
    - indexing, length
    - reverse, head, tail, reverse-head, reverse-tail
    - concatenation
    - split-by-item, split-by-sublist, split-at-index, split-at-indices, split-into-n, split-every-n
    - index-of-item, index-of-sublist
    - interleave, uninterleave, uninterleave-into-n
    - range-from-1, range-from-0, range, indices, symmetric-range
    - depth
    - head-tail, reverse-head-tail
    - every-nth, repeat, repeat-items
    - flatten, flatten-once, mold
    - transpose, transpose-with-filler
    - pad-left, pad-right, pad-center
    - trim-left, trim-right, trim-both
    - random-choice
    - min-of, max-of, sort, uniquify
    - all, any, none
    - permutations, nth-permutation, permutation-index, nth-permutation-of, permutation-index-of
    - n-combinations, n-combinations-with-replacement, n-permutations
    - partitions, powerset, prefixes, suffixes
    - palindromize, mirror, is-palindrome
    - run-length-encode, run-length-decode
    - rotate-right, rotate-left
    - intersection, set-difference, set-xor
    - multiset-intersection, multiset-difference, multiset-xor, multiset-union
- String manipulation
    - chr, ord
    - split-by-whitespace, split-lines
    - join-by, join-by-space, join-by-newline
    - regex-groups, regex-replace, regex-replace-func
- Math
    - add, subtract, multiply, int-divide, divide, mod, power
    - increment, decrement, halve, double, square, square-root
    - abs, exp, log, exp2, log2, logn, factorial
    - round, floor, ceil
    - sin, cos, tan, asin, acos, atan, atan2
    - power-mod, symmetric-mod
    - vector distance, length
    - to-decimal, to-binary, to-base, from-decimal, from-binary, from-base
    - bitwise-and, bitwise-or, bitwise-not, bitwise-xor, shift-left, shift-right
    - min, max
    - insignificant
    - gcd, lcm
    - nth-prime, primes-until, n-primes, next-prime, is-prime
    - prime-factors, prime-exponents, divisors, proper-divisors
    - determinant, norm, matrix multiply, matrix power, diagonals, un-diagonals
- I/O
    - read-line, read-eval, print-line, print
    - read-image, write-image, read-url
- Logical/comparison
    - not, or, and (short circuiting)
    - equal, not-equal
    - less-than, greater-than, less-or-equal, greater-or-equal
- Miscellaneous/debug
    - eval, repr, json-encode, json-parse
    - sleep, date, time
- more should be copied from Jelly or invented when these are done
