# Perthensis

Perthensis is a language intended for [code golf](https://en.wikipedia.org/wiki/Code_golf) and created by [PPCG.SE](https://codegolf.stackexchange.com) user [Pietu1998](http://codegolf.stackexchange.com/users/30164/pietu1998).

Perthensis is imperative like [Pyth](https://github.com/isaacg1/pyth), but provides functions and data types like [Jelly](https://github.com/DennisMitchell/jellylanguage).

Perthensis was named after the smallest python in the world, [_Antaresia perthensis_](https://en.wikipedia.org/wiki/Antaresia_perthensis).

## Design

Perthensis was designed to complement Jelly and Pyth, two code golf languages I have used extensively in the last couple of years.

Jelly can be hard to program in because of its tacit paradigm. More complex algorithms can be nearly impossible to implement in Jelly, because there are no variables and the execution order of functions depends on many things.

On the other hand, Pyth is much easier to program in, but hasn't been updated in a long time, only uses a subset of the code page, and has many properties that negatively affect code golf.

Perthensis aims to fix these issues by combining explicit argument specification with a rich function library.

More technical details can be found in [DESIGN.md](./DESIGN.md).
